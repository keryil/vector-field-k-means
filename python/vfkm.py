#!/usr/bin/env python

from subprocess import call
from os import path
import os
from collections import namedtuple
import matplotlib
print matplotlib.use("QT4Agg")
from matplotlib import pyplot as plt
from math import sqrt
import math

BoundingBox = namedtuple("BoundingBox", ["left", "right", "bottom", "top", "start", "end"])
Point = namedtuple("Point", ["x", "y", "time"])

def parse_trajectory_file(trajectoryFile):
    """
    Opens a trajectory files and returns the bounding box 
    plus trajectories. 
    """
    # left = right = bottom = top = start = end = None
    box = None
    trajectories = []
    with open(trajectoryFile) as f:
        lines = f.readlines()
        # first, the bounding box
        line = lines[0].rstrip().split(" ")
        box = BoundingBox(*tuple([float(x) for x in line]))
        trajectory = []
        for line in lines[1:]:
            line = line.rstrip()
            # this is the trajectory delimiter
            if line == "0.0 0.0 0.0":
                trajectories.append(trajectory)
                trajectory = []
            else:
                trajectory.append(Point(*tuple([float(x) for x in line.split(" ")])))

    return box, trajectories

def parse_vector_field_file(vectorfieldFile):
    field = []
    trajectories = []
    # prefix = os.sep.join(vectorfieldFile.split(os.sep)[:-1])
    with open(vectorfieldFile) as fieldFile:
            for line in fieldFile.readlines()[1:]:
                field.append(tuple([float(x) for x in line.rstrip().split(" ")]))

    field = tuple(field)
    curveFile = vectorfieldFile.replace("vf", "curves")
    with open(curveFile) as curveFile:
        for line in curveFile.readlines():
            trajectories.append(int(line.split(" ")[0]))
    return field, trajectories

def plot_trajectory(trajectory):
    plot_quiver2d([(p.x, p.y) for p in trajectory])
    # plt.plot(x,y)

def plot_trajectories(trajectories, show=True):
    for t in trajectories:
        plot_trajectory(t)
    if show:
        plt.show()

def plot_quiver2d(data):
    X, Y = zip(*tuple(data))
    U = [x1-x0 for x0,x1 in zip(X[:-1],X[1:])]
    V = [y1-y0 for y0,y1 in zip(Y[:-1],Y[1:])]
    color_delta = 1. / (len(X) - 1)
    C = [(0,int(color_delta*i),0) for i in range(len(X))]
#     print C
    X, Y = X[:-1], Y[:-1]
#     print X, Y, U, V
    plt.quiver(X, Y, U, V, C, scale_units='xy',angles='xy', scale=1, width=0.007, alpha=0.75, edgecolors=("black"))

def plot_vector_field(field, box, show=True, subplot=None, *args, **kwargs):
    """
    Given a vector field, which is comprised of n tuples of coordinates,
    plots the flow field representation.
    """
    resolution = sqrt(float(len(field)))
    x_tick = float(box.right - box.left)/resolution
    y_tick = float(box.top - box.bottom)/resolution

    X = []
    Y = []
    U = []
    V = []
    for i, (x,y) in enumerate(field):
        calc_x = box.left + x_tick * (i % resolution)
        calc_y = box.bottom + y_tick * (math.floor(i / resolution))
        X.append(calc_x)
        Y.append(calc_y)
        u, v = -x, -y
        # if abs(x-u) > resolution / 2:
        #     if x > 0:
        #         u = x - resolution /2 
        #     else:
        #         x = x + resolution / 2
        
        # if abs(y-v) > resolution / 2:
        #     if y > 0:
        #         v = y - resolution / 2
        #     else:
        #         y = y + resolution / 2
                

        U.append(u)
        V.append(v)
    scale = max(sqrt(a**2 + b**2) for a, b in zip(U, V)) / resolution / 2
    plt.quiver(X,Y,U,V,scale=scale,angles="xy",scale_units='xy',width=0.005,
                alpha=.45,headwidth=4,
                edgecolors=("k"), *args,**kwargs)
    if show:
        if subplot != None:
            subplot.show()
        else:
            plt.show()

def plot_vector_fields(fields, box, show=True, memberships=None):
    colors = ["red", "green", "orange", "blue", "pink", "purple"]
    import math
    x = y = int(math.ceil(sqrt(len(fields)+1)))
    # x, y = 2, 2
    f, axes = plt.subplots(x,y)
    prefix = "%s%s" % (x, y)
    # order = ['221','222','223','224']
    for i, (field, color) in enumerate(zip(fields, colors)):
        print "Plotting field %s" % i
        plt.figure(1)
        title = "Category %s"
        if memberships is not None:
            title = title % ("%i (%i members)" % (i, len(memberships[field])))
        else:
            title = title % i
        plt.subplot("%s%d" % (prefix, i+1)).set_title(title)
        plot_vector_field(field, box, show=False, color=color)
        if memberships is not None:
            plot_trajectories(memberships[field], show=False)
    plt.subplot("%s%s" % (prefix,x**2)).set_title("All trajectories (%i)" % sum([len(memberships[field]) for field in fields]))
    if memberships is not None:
        for field in memberships:
            plot_trajectories(memberships[field], show=False)
    if show:
        plt.show()


def run_vfkm_from_file(trajectoryFile, gridResolution, numberOfVectorFields, smoothnessWeight, outputDirectory):
    """
    Clusters the given trajectories based on a given grid resolution, target clusters (vector fields) and the 
    smoothness weight. The output files are placed in a given directory, which is created if it does not exist.

    """
    if os.path.isdir(outputDirectory):
        import shutil
        shutil.rmtree(outputDirectory)
        os.remove("msr_campus_cropped.txt")
    os.mkdir(outputDirectory)
    command = path.join("..", "src", "vfkm")
    args = [trajectoryFile, str(gridResolution), str(numberOfVectorFields), str(smoothnessWeight), outputDirectory]
    full = [command]
    full.extend(args)
    full = " ".join(full)
    print "Executing command: %s" % full
    return call(full, shell=True)


data_file = "../../LeapArticulator/heikki.trajectories"
# data_file = "../data/synthetic.txt"
# ./vfkm trajectoryFile gridResolution numberOfVectorFields smoothnessWeight outputDirectory
print run_vfkm_from_file(data_file, 30, 3, 0.05, "./finalOutput")

from glob import glob
fields = glob("finalOutput/vf_r_*.txt")

box, trajectories = parse_trajectory_file(data_file)
vf = []
print len(trajectories), "trajectories in total."
memberships = {}
for a, f in enumerate(fields):
    print "Reading vector field", f
    field, membership = parse_vector_field_file(f)
    # print "**********\n", a, field
    vf.append(field)
    membership = [trajectories[i] for i in membership]
    memberships[field] = membership
# print memberships
plot_vector_fields(vf, box, memberships=memberships, show=False)
# plt.figure(1)
# plt.subplot("223")
# plot_trajectories(trajectories)
plt.show()   
